package com.supanut.midfin;

import javax.swing.JFrame;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class App1 extends JFrame {
    JLabel HowToPlay1, HowToPlay2;
    JButton Next;

    public App1() {
        super("Boss Fight Game");
        this.setSize(300, 400);

        HowToPlay1 = new JLabel("How To Play");
        HowToPlay1.setBounds(60, 10, 265, 50);
        HowToPlay1.setFont(new Font("Serif", Font.PLAIN, 30));

        HowToPlay2 = new JLabel("<html>### You must defeat the Boss. ###<br><br> 'Attack' can deal damage = 100 <br> after that Boss will deal damage = 20 to you. <br> 'Magic' can deal damage = 300 <br> and you will lose MP = 5. <br> 'Potion' can  heal your HP = 30 <br> and you will lose MP = 2.<br> *** Be Careful *** <br> If you use 'Magic' and 'Potion' when you run out of MP, it will have no effect.<br>and you will get hit by Boss. </html>");
        HowToPlay2.setBounds(10, 70, 265, 200);

        Next = new JButton("Fight Boss");
        Next.setBounds(10, 280, 265, 50);
        Next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        this.add(HowToPlay1);
        this.add(HowToPlay2);
        this.add(Next);

        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        App1 game = new App1();
    }
}
