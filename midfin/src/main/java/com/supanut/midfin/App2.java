package com.supanut.midfin;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class App2 extends JFrame {
    JButton Attack, Magic, Potion;
    JLabel BossName, BossHP, PlayerHP;
    public static int BHP = 1000, PHP = 100, PMP = 10;

    public App2() {
        super("Boss Fight Game");
        this.setSize(300, 400);

        Attack = new JButton("Attack");
        Attack.setBounds(10, 220, 80, 80);
        Attack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BHP -= 100;
                PHP -= 20;
                BossHP.setText("Boss HP :  " + BHP + "   /   1000");
                PlayerHP.setText("Player HP  :  " + PHP + "   /   100" + "     MP  :  " + PMP + "   /   10");
            }
        });
        Magic = new JButton("Magic");
        Magic.setBounds(103, 220, 80, 80);
        Magic.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MagicCheckMP();
                BossHP.setText("Boss HP :  " + BHP + "   /   1000");
                PlayerHP.setText("Player HP  :  " + PHP + "   /   100" + "     MP  :  " + PMP + "   /   10");
            }
        });

        Potion = new JButton("Potion");
        Potion.setBounds(195, 220, 80, 80);
        Potion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PotionCheckMP();
                PotionCheckHP();
                BossHP.setText("Boss HP :  " + BHP + "   /   1000");
                PlayerHP.setText("Player HP  :  " + PHP + "   /   100" + "     MP  :  " + PMP + "   /   10");
            }
        });

        PlayerHP = new JLabel("Player HP  :  " + PHP + "   /   100" + "     MP  :  " + PMP + "   /   10");
        PlayerHP.setBounds(10, 320, 265, 25);

        BossHP = new JLabel("Boss HP :  " + BHP + "   /   1000");
        BossHP.setBounds(78, 140, 265, 25);

        BossName = new JLabel("");
        BossName.setBounds(80, 10, 128, 128);
        BossName.setIcon(new ImageIcon("gargoyle.png"));

        this.add(Attack);
        this.add(Magic);
        this.add(Potion);

        this.add(PlayerHP);
        this.add(BossHP);
        this.add(BossName);

        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void MagicCheckMP() {
        if (PMP <= 4) {
            PMP -= 0;
            BHP -= 0;
            PHP -= 20;
        } else {
            BHP -= 300;
            PMP -= 5;
        }
    }

    public void PotionCheckMP() {
        if (PMP <= 1) {
            PMP -= 0;
            PHP -= 20;
        } else {
            PHP += 30;
            PMP -= 2;
        }
    }

    public void PotionCheckHP() {
        if (PHP <= 0) {
            PHP = 0;
        }
        if (PHP >= 100) {
            PHP = 100;
        }
    }

    public static void main(String[] args) {
        App2 game = new App2();
    }
}
