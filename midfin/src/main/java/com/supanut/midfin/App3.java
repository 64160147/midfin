package com.supanut.midfin;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class App3 extends JFrame {
    JLabel TextDisplay;
    JButton BackToTitle, NewGame, ExitGame2;;

    public App3() {
        super("Boss Fight Game");
        this.setSize(300, 400);

        TextDisplay = new JLabel("");
        TextDisplay.setBounds(30, 120, 250, 80);
        TextDisplay.setFont(new Font("Serif", Font.PLAIN, 35));

        BackToTitle = new JButton("Back To Title");
        BackToTitle.setBounds(10, 245, 265, 50);
        BackToTitle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        NewGame = new JButton("Play Game");
        NewGame.setBounds(10, 190, 265, 50);
        NewGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        ExitGame2 = new JButton("Exit Game");
        ExitGame2.setBounds(10, 300, 265, 50);
        ExitGame2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        this.add(NewGame);
        this.add(ExitGame2);
        this.add(TextDisplay);
        this.add(BackToTitle);

        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        App3 game = new App3();

    }
}
