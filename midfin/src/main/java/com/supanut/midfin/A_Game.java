package com.supanut.midfin;

import javax.swing.JFrame;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class A_Game extends JFrame {
    JLabel GameName, Picture, HowToPlay1, HowToPlay2, BossName, BossHP, PlayerHP, TextDisplay;
    JButton PlayGame, ExitGame, Next, Attack, Magic, Potion, BackToTitle, NewGame, ExitGame2;
    public static int BHP = 1000, PHP = 100, PMP = 10;

    public A_Game() {
        super("Boss Fight Game");
        this.setSize(300, 400);
        GameName = new JLabel("Abyssal Arena");
        GameName.setBounds(30, 0, 265, 90);
        GameName.setFont(new Font("Serif", Font.PLAIN, 40));

        Picture = new JLabel("");
        Picture.setBounds(80, 65, 265, 90);
        Picture.setIcon(new ImageIcon("cat-eyes.png"));

        PlayGame = new JButton("Play Game");
        PlayGame.setBounds(10, 230, 265, 50);
        PlayGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BeginPlay();
            }
        });

        ExitGame = new JButton("Exit Game");
        ExitGame.setBounds(10, 290, 265, 50);
        ExitGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        HowToPlay1 = new JLabel("How To Play");
        HowToPlay1.setBounds(60, 10, 265, 50);
        HowToPlay1.setFont(new Font("Serif", Font.PLAIN, 30));

        HowToPlay2 = new JLabel("<html>### You must defeat the Boss. ###<br><br> 'Attack' can deal damage = 100 <br> after that Boss will deal damage = 20 to you. <br> 'Magic' can deal damage = 300 <br> and you will lose MP = 5. <br> 'Potion' can  heal your HP = 30 <br> and you will lose MP = 2.<br> *** Be Careful *** <br> If you use 'Magic' and 'Potion' when you run out of MP, it will have no effect.<br>and you will get hit by Boss. </html>");
        HowToPlay2.setBounds(10, 70, 265, 200);

        Next = new JButton("Fight Boss");
        Next.setBounds(10, 290, 265, 50);
        Next.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BHP = 1000;
                PHP = 100;
                PMP = 10;
                BossHP.setText("Boss HP :  " + BHP + "   /   1000");
                PlayerHP.setText("Player HP  :  " + PHP + "   /   100" + "     MP  :  " + PMP + "   /   10");
                BossName.setIcon(new ImageIcon("gargoyle.png"));
                FightBoss();
            }
        });

        Attack = new JButton("Attack");
        Attack.setBounds(10, 220, 80, 80);
        Attack.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BHP -= 100;
                PHP -= 20;
                CheckRulesGame();
                BossHP.setText("Boss HP :  " + BHP + "   /   1000");
                PlayerHP.setText("Player HP  :  " + PHP + "   /   100" + "     MP  :  " + PMP + "   /   10");
            }
        });
        Magic = new JButton("Magic");
        Magic.setBounds(103, 220, 80, 80);
        Magic.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                MagicCheckMP();
                CheckRulesGame();
                BossHP.setText("Boss HP :  " + BHP + "   /   1000");
                PlayerHP.setText("Player HP  :  " + PHP + "   /   100" + "     MP  :  " + PMP + "   /   10");
            }
        });

        Potion = new JButton("Potion");
        Potion.setBounds(195, 220, 80, 80);
        Potion.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                PotionCheckMP();
                PotionCheckHP();
                CheckRulesGame();
                BossHP.setText("Boss HP :  " + BHP + "   /   1000");
                PlayerHP.setText("Player HP  :  " + PHP + "   /   100" + "     MP  :  " + PMP + "   /   10");
            }
        });

        PlayerHP = new JLabel("Player HP  :  " + PHP + "   /   100" + "     MP  :  " + PMP + "   /   10");
        PlayerHP.setBounds(10, 320, 265, 25);

        BossHP = new JLabel("Boss HP :  " + BHP + "   /   1000");
        BossHP.setBounds(78, 140, 265, 25);

        BossName = new JLabel("");
        BossName.setBounds(80, 10, 128, 128);
        BossName.setIcon(new ImageIcon("gargoyle.png"));

        TextDisplay = new JLabel("");
        TextDisplay.setBounds(30, 120, 250, 80);
        TextDisplay.setFont(new Font("Serif", Font.PLAIN, 35));

        BackToTitle = new JButton("Back To Title");
        BackToTitle.setBounds(10, 245, 265, 50);
        BackToTitle.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BackToTitle();
            }
        });

        NewGame = new JButton("New Game");
        NewGame.setBounds(10, 190, 265, 50);
        NewGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                BHP = 1000;
                PHP = 100;
                PMP = 10;
                BossHP.setText("Boss HP :  " + BHP + "   /   1000");
                PlayerHP.setText("Player HP  :  " + PHP + "   /   100" + "     MP  :  " + PMP + "   /   10");
                BossName.setIcon(new ImageIcon("gargoyle.png"));
                NewGame();
            }
        });

        ExitGame2 = new JButton("Exit Game");
        ExitGame2.setBounds(10, 300, 265, 50);
        ExitGame2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        this.add(GameName);
        this.add(Picture);
        this.add(PlayGame);
        this.add(ExitGame);

        this.add(HowToPlay1);
        this.add(HowToPlay2);
        this.add(Next);

        this.add(Attack);
        this.add(Magic);
        this.add(Potion);
        this.add(PlayerHP);
        this.add(BossHP);
        this.add(BossName);

        this.add(NewGame);
        this.add(ExitGame2);
        this.add(TextDisplay);
        this.add(BackToTitle);

        JLabel[] labels = new JLabel[] { HowToPlay1, HowToPlay2, PlayerHP, BossHP, BossName, TextDisplay };
        for (JLabel label : labels) {
            label.setVisible(false);
        }
        JButton[] buttons = new JButton[] { Next, Attack, Magic, Potion, NewGame, ExitGame2, BackToTitle };
        for (JButton button : buttons) {
            button.setVisible(false);
        }

        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public void BeginPlay() {
        JLabel[] labels = new JLabel[] { HowToPlay1, HowToPlay2 };
        for (JLabel label : labels) {
            label.setVisible(true);
        }
        JButton[] buttons = new JButton[] { Next };
        for (JButton button : buttons) {
            button.setVisible(true);
        }
        JLabel[] labels2 = new JLabel[] { GameName, Picture };
        for (JLabel label2 : labels2) {
            label2.setVisible(false);
        }
        JButton[] buttons2 = new JButton[] { PlayGame, ExitGame };
        for (JButton button2 : buttons2) {
            button2.setVisible(false);
        }
    }

    public void FightBoss() {
        JLabel[] labels = new JLabel[] { HowToPlay1, HowToPlay2 };
        for (JLabel label : labels) {
            label.setVisible(false);
        }
        JButton[] buttons = new JButton[] { Next };
        for (JButton button : buttons) {
            button.setVisible(false);
        }

        JLabel[] labels2 = new JLabel[] { PlayerHP, BossHP, BossName };
        for (JLabel label2 : labels2) {
            label2.setVisible(true);
        }
        JButton[] buttons2 = new JButton[] { Attack, Magic, Potion };
        for (JButton button2 : buttons2) {
            button2.setVisible(true);
        }
    }

    public void MagicCheckMP() {
        if (PMP <= 4) {
            PMP -= 0;
            BHP -= 0;
            PHP -= 20;
        } else {
            BHP -= 300;
            PMP -= 5;
        }
    }

    public void PotionCheckMP() {
        if (PMP <= 1) {
            PMP -= 0;
            PHP -= 20;
        } else {
            PHP += 30;
            PMP -= 2;
        }
    }

    public void PotionCheckHP() {
        if (PHP <= 0) {
            PHP = 0;
        }
        if (PHP >= 100) {
            PHP = 100;
        }
    }

    public void CheckRulesGame() {
        if (PHP <= 0) {
            JLabel[] labels = new JLabel[] { PlayerHP, BossHP };
            for (JLabel label : labels) {
                label.setVisible(false);
            }
            JButton[] buttons = new JButton[] { Attack, Magic, Potion };
            for (JButton button : buttons) {
                button.setVisible(false);
            }

            JLabel[] labels2 = new JLabel[] { TextDisplay };
            for (JLabel label2 : labels2) {
                label2.setVisible(true);
            }
            JButton[] buttons2 = new JButton[] { NewGame, ExitGame2, BackToTitle };
            for (JButton button2 : buttons2) {
                button2.setVisible(true);
            }
            BossName.setIcon(new ImageIcon("skull.png"));
            NewGame.setText("Try Again");
            ExitGame2.setText("Give up");
            TextDisplay.setText("GAME  OVER");
            TextDisplay.setFont(new Font("Serif", Font.PLAIN, 35));
        }
        if (BHP <= 0) {
            JLabel[] labels = new JLabel[] { PlayerHP, BossHP };
            for (JLabel label : labels) {
                label.setVisible(false);
            }
            JButton[] buttons = new JButton[] { Attack, Magic, Potion };
            for (JButton button : buttons) {
                button.setVisible(false);
            }
            JLabel[] labels2 = new JLabel[] { TextDisplay };
            for (JLabel label2 : labels2) {
                label2.setVisible(true);
            }
            JButton[] buttons2 = new JButton[] { NewGame, ExitGame2, BackToTitle };
            for (JButton button2 : buttons2) {
                button2.setVisible(true);
            }
            BossName.setIcon(new ImageIcon("ritual.png"));
            NewGame.setText("Play Again");
            ExitGame2.setText("Exit Game");
            TextDisplay.setText("Monster have been defeated");
            TextDisplay.setFont(new Font("Serif", Font.PLAIN, 20));
        }
    }

    public void NewGame() {
        JLabel[] labels = new JLabel[] { TextDisplay };
        for (JLabel label : labels) {
            label.setVisible(false);
        }
        JButton[] buttons = new JButton[] { NewGame, ExitGame2, BackToTitle };
        for (JButton button : buttons) {
            button.setVisible(false);
        }

        JLabel[] labels2 = new JLabel[] { PlayerHP, BossHP, BossName };
        for (JLabel label2 : labels2) {
            label2.setVisible(true);
        }
        JButton[] buttons2 = new JButton[] { Attack, Magic, Potion };
        for (JButton button2 : buttons2) {
            button2.setVisible(true);
        }
    }

    public void BackToTitle() {

        JLabel[] labels = new JLabel[] { TextDisplay, BossName };
        for (JLabel label : labels) {
            label.setVisible(false);
        }
        JButton[] buttons = new JButton[] { NewGame, ExitGame2, BackToTitle };
        for (JButton button : buttons) {
            button.setVisible(false);
        }

        JLabel[] labels2 = new JLabel[] { GameName, Picture };
        for (JLabel label2 : labels2) {
            label2.setVisible(true);
        }
        JButton[] buttons2 = new JButton[] { PlayGame, ExitGame };
        for (JButton button2 : buttons2) {
            button2.setVisible(true);
        }
    }

    public static void main(String[] args) {
        A_Game ProjactA = new A_Game();
    }
}
