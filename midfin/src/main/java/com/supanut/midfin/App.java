package com.supanut.midfin;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

public class App extends JFrame {
    JLabel GameName, Picture;
    JButton PlayGame, ExitGame;

    public App() {
        super("Boss Fight Game");
        this.setSize(300, 400);

        GameName = new JLabel("Abyssal Arena");
        GameName.setBounds(30, 0, 265, 90);
        GameName.setFont(new Font("Serif", Font.PLAIN, 40));

        Picture = new JLabel("");
        Picture.setBounds(80, 65, 265, 90);
        Picture.setIcon(new ImageIcon("cat-eyes.png"));

        PlayGame = new JButton("Play Game");
        PlayGame.setBounds(10, 230, 265, 50);
        PlayGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

            }
        });

        ExitGame = new JButton("Exit Game");
        ExitGame.setBounds(10, 290, 265, 50);
        ExitGame.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.exit(0);
            }
        });

        

        this.add(GameName);
        this.add(Picture);

        this.add(PlayGame);
        this.add(ExitGame);

        this.setLayout(null);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setVisible(true);
    }

    public static void main(String[] args) {
        App game = new App();
    }
}
